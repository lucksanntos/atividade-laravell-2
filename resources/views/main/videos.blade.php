@extends('layouts.home')
@section('title','Vídeos')
@section('conteudo')
    <div class="row">
        <div class="title col-md-12 text-center mx auto">
            <h3>VIDEO AULAS</h3>
        </div>
    </div>
    <div class="row">
        <div class="item col-4">
            <iframe class="img-fluid" width="560" height="315" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="item1 col-8">
            <h4>Cursode HTML e CSS para iniciantes - Aula 1</h4>
            <p>Nesta primeira aula vou passar o conceito básico de HTML e mostrar como criar sua primeira página para teste. Com esta página você vai aprender a criar a estrutura mais simples de um documento HTML, definir títulos e também a codificação(charset) para seus documentos.</p>
        </div>
    </div>
    <div class="row">
        <div class="item2 col-4">
        <iframe class="img-fluid"  width="560" height="315" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>       
        <h5>GitHub, gitLab ou bitbucket? qual nós usamos! // CAC#6</h5>
        </div>

        <div class="item3 col-4">
        <iframe class="img-fluid" width="560" height="315" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h5>MVC // Dicionário do Programador</h5>
        </div>

        <div class="item4 col-4">
        <iframe class="img-fluid" width="560" height="315" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>       
        <h5>O que um Analista de Sistemas faz? // Vlog #77</h5>
        
        </div>
    </div>
@endsection