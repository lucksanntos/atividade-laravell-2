@extends('layouts.home')
@section('title','Contato')
@section('conteudo')

<div class="row">
    <div class="col-md-12 text-center">
        <h1>Contato</h1> 
    </div>
</div>

<div class="row">
    <div class="col-10">
        <form>
            <div class="form-group">
                <label for="name"></label>
                <input type="text" class="form-control" id="name" placeholder="Nome...">
            </div>
            <div class="form-group">
                <label for="tel"></label>
                <input type="text" class="form-control" id="tel" placeholder="Telefone...">
            </div>
            <div class="form-group">
                <label for="email"></label>
                <input type="email" class="form-control" id="email" placeholder="E-mail...">
            </div>
            <div class="form-group">
                <label for="textarea"></label>
                <textarea class="form-control" id="txtarea" placeholder="Mensagem?" rows="3"></textarea>
            </div>
            <div class="form-group">
            <button type="button" class="btn btn-primary">Enviar</button>
            </div>
        </form>

    </div>  
    <form class="dd col-md-2">
        <div class="form-group">
            <h6>Marília:</h6>
            <p>Av Sampaio Vidal 587</p>
            <p>Phone: +14 1292 0991</p>
            <a href="#">suport@mysite.com</a>
            
        </div>    
        <div class="form-group">
            <br>
            <h6>São Paulo:</h6>
            <p>Company inc.</p>
            <p>Av Paulista 201</p>
            <p>Centro, SP</p>
            <p>Phone: +11 1478 2587</p>
            <a href="#">usa@mysite.com</a>
        </div>

    </form>
</div>

@endsection