<?php


Route::get('/', function () {
    return view('main.home');
})->name('home');

Route::get('html',function(){
    return view('main.html');
})->name('html');

Route::get('javascript',function(){
    return view('main.javascript');
})->name('javascript');

Route::get('tema-css',function(){
    return view('main.tema-css');
})->name('tema-css');

Route::get('videos',function(){
    return view('main.videos');
})->name('videos');

Route::get('contato',function(){
    return view('main.contato');
})->name('contato');

